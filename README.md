# Database Class #

A simple and easy to use database abstraction for mysqli.

### Requirements ###

* PHP >= 5.3
* mysqli extension

### Usage ###

First, you've to add this to your [`composer.json`](http://getcomposer.org/) dependencies:

```
"require": {
  "shirerom/databaseclass": ">=1.0"
}
```

and run

```bash
composer update
```

Now you can create a new database object and request data from the database:

```
#!php
require_once "vendor/autoload.php";

$D = new Database("localhost", "dbuser", "dbpassword", "dbname");

$resultAssoc = $D->execute("SELECT id, name FROM Task")->getRowsAll();
```

### License ###

The MIT License (MIT)

Copyright (c) 2015 Johannes Bauer

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.