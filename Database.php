<?php

/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2017 Johannes Bauer <mail@bauerjohannes.de>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


class DatabaseException extends Exception {}



/**
 * A simple and easy to use database abstraction for mysqli
 */
class Database extends mysqli {

	const W_OR = "OR";
	const W_AND = "AND";

	/**
	 * Construkctor
	 *
	 * @param string $hostname
	 * @param string $username
	 * @param string $password
	 * @param string $database
	 */
	public function __construct($hostname, $username, $password = "", $database = "") {
		
		parent::__construct($hostname, $username, $password, $database);

		$this->query("set autocommit=1");
	}

	/**
	 * begins a transaction
	 */
	public function begin() {

		$this->query("set autocommit=0");
		$this->query("START TRANSACTION");
	}

	/**
	 * commits a transaction
	 */
	public function commit($flags = NULL, $name = NULL) {

		$this->query("COMMIT");
		$this->query("set autocommit=1");
	}

	/**
	 * roll back a transaction
	 */
	public function rollback($flags = NULL, $name = NULL) {

		$this->query("ROLLBACK");
		$this->query("set autocommit=1");
	}

	/**
	 * executes a sql query
	 *
	 * @param string $query
	 * @return mixed
	 *
	 * @throws DatabaseException
	 */
	public function execute($query) {

		$value = parent::query($query);

		// boolscher Rückgabewert
		// → INSERT, UPDATE, DELETE
		if(is_bool($value) and $value === true) {
			return $value;
		}

		// Object als Rückgabewert
		// → SELECT, SHOW, EXPLAIN
		elseif(is_object($value)){
			return new DatabaseResult($value);
		}

		else {
			throw new DatabaseException($this->error, $this->errno);
		}
	}

	/**
	 * closes a database connection
	 */
	public function close() {
		parent::close();
	}

	/**
	 * 
	 * @return int
	 */
	public function getInsertId() {
		return $this->insert_id;
	}
	
	public function insert($table) {
		return new DatabaseInsert($this, $table);
	}

	public function select($table) {
		return new DatabaseSelect($this, $table);
	}
}

class DatabaseResult {

	protected $result;

	public function __construct(mysqli_result $result) {
		$this->result = $result;
	}

	public function getRow() {
		return $this->result->fetch_assoc();
	}

	public function getRowsAll() {

		$rows = array();

		while($row = $this->result->fetch_assoc())  {
			$rows[] = $row;
		}

		return $rows;
	}
}


class DatabaseInsert {
	
	/**
	 * 
	 * @var Database
	 */
	var $database;
	
	/**
	 *
	 * @var string
	 */
	var $table;
	
	/**
	 *
	 * @var array 
	 */
	var $values;
	
	/**
	 *
	 * @var string 
	 */
	static $template = "INSERT INTO %s SET %s";


	public function __construct(Database $database, $table) {
		$this->database = $database;
		$this->table = $table;
	}
	
	public function values(array $values) {
		
		$this->values = $values;
		
		$this->database->execute($this->getSqlStatement());

		return $this;
	}
	
/*	public function confirm() {
		$this->database->execute($this->getSqlStatement());
	}*/
		
	protected function getSqlStatement() {
		
		if (empty($this->values)) {
			throw new DatabaseException("no values set");
		}
		
		$sqlValues = [];
		
		foreach ($this->values as $field => $value) {
			$sqlValues[] = "{$field} = {$value}";
		}
		
		return sprintf(self::$template, $this->table, implode(", ", $sqlValues));
	}
}

class DatabaseSelect {
	
	/**
	 * 
	 * @var Database
	 */
	var $database;
	
	/**
	 *
	 * @var string
	 */
	var $table;
	
	/**
	 *
	 * @var array 
	 */
	var $values;
	
	/**
	 *
	 * @var mixed
	 */
	var $where;

	/**
	 *
	 * @var string
	 */
	var $order;
	
	/**
	 *
	 * @var int
	 */
	var $limit;
	
	/**
	 *
	 * @var int
	 */
	var $limitOffset = 0;
	
	/**
	 *
	 * @var string 
	 */
	static $template = "SELECT %s FROM %s %s %s %s";
	
	
	public function __construct(Database $database, $table) {
		$this->database = $database;
		$this->table = $table;
	}
	
	public function values(array $values) {
		
		$this->values = $values;

		return $this;
		
/*		$sqlWhere = "";
		
		$where = [
			Database::W_OR => [
						"name" => "hallo",
						"id" => 1,
						Database::W_AND => [
								"value1" => true,
								"value2" => "something"
						]
				]
		];
		
		$whereString = "name = 'hallo' OR id = 1 OR (value1 = 1 AND value2 = 'something')";
		
		if (is_array($this->where)) {
			
			$sqlWhere .= "WHERE ";
			
			foreach ($this->where as $field => $value) {
				
			}
		}
 */
	}
	
	public function where($where) {
		
		$this->where = $where;
		
		return $this;
	}

	public function order($order) {
	
		$this->order = $order;
		
		return $this;
	}
	
	public function limit($limit, $offset = 0) {
		
		$this->limit = $limit;
		$this->limitOffset = $offset;
		
		return $this;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function all() {
		return $this->database->execute($this->getSqlStatement())->getRowsAll();
	}
	
	public function first() {		
		return $this->database->execute($this->getSqlStatement())->getRow();
	}
	
	public function handle() {				
		return $this->database->execute($this->getSqlStatement());
	}
	
	protected function getWhereString() {
		
		if (!$this->where) {
			return "";
		}
		
		if (is_array($this->where)) {
			throw new DatabaseException("not implemented");
		}
		
		return "WHERE {$this->where}";
	}
	
	protected function getOrderString() {
		
		if (!$this->order) {
			return "";
		}
		
		if (is_array($this->order)) {
			throw new DatabaseException("not implemented");
		}
		
		return "ORDER BY {$this->order}";
	}
	
	protected function getLimitString() {
		
		if (!$this->limit) {
			return "";
		}
		
		return "LIMIT {$this->limitOffset}, {$this->limit}";
	}
	
	protected function getSqlStatement() {
		return sprintf(self::$template, implode(", ", $this->values), $this->table, $this->getWhereString(), $this->getOrderString(), $this->getLimitString());
	}
}